package ru.dsherstobitov.games

import java.util
import java.util.TimerTask
import javax.swing.{JFrame, SwingUtilities}

/**
 * Author: dsherstobitov 26.10.14.
 */
object Main extends App {

  //размерность игрового поля
  val worldSize = 1024

  //обработчик событий игрового поля
  val worldListener = new WorldListener {
    def onChangeGeneration(generation: BitSetWrapper, activeZone: ActiveZone) = {
      SwingUtilities.invokeLater(new Runnable {
        override def run() = frame.setDrawSource(generation, activeZone)
      })
    }
  }

  //обработчик событий графического интерфейса
  val guiListener = new GuiListener {
    def onPlayButton(isPaused: Boolean) = gameTimer.isPaused = isPaused
    def onCellSwitch(row: Int, col: Int) = game.reverseState(row, col)
    def onRandomCall() = game.randomiseState()
    def onRefreshTimeChanged(time: Int, isPaused: Boolean) = {
      gameTimer.cancel()
      gameTimer = new GameTimer(game, time, isPaused)
    }
  }

  var frame : MainFrame = null
  SwingUtilities.invokeAndWait(new Runnable {
    override def run() = {
      frame = new MainFrame(guiListener, worldSize)
      frame.setTitle("Game Of Life")
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
      frame.setVisible(true)
    }
  })

  val game : GameController = GameController(worldListener, worldSize, (55, 55), (55, 56), (55, 57), (54, 57), (53, 56) )
  var gameTimer : GameTimer = new GameTimer(game, 50, true)
}

class GameTimer(val game: GameController, val period : Int, var isPaused: Boolean) extends util.Timer("game timer") {

  schedule(new TimerTask {
    def run() = {
      if (!isPaused) {
        game.makeDecisions()
        game.releaseDecisions()
      }
    }
  } , 50, period)

}



