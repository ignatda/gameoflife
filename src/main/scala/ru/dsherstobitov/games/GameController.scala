package ru.dsherstobitov.games

import java.util.concurrent.TimeUnit

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future, TimeoutException}
import scala.util.Random

/**
 * Author: dsherstobitov 26.10.14.
 */
class GameController(val worldListener: WorldListener, val worldSize: Int) {
  //текущее состояние
  private val state = new BitSetWrapper(worldSize)
  //следующее состояние
  private val nextState = new BitSetWrapper(worldSize)

  //текущая активная зона
  private val activeZone = new ActiveZone(worldSize, worldSize)
  //следующая активная зона
  private val nextActiveZone = new ActiveZone(worldSize, worldSize)

  /** Изменяет состояние точки на поле (текущее состояние)
    * @param row индекс строки
    * @param col индекс столбца
    * @return this GameController
    */
  def reverseState(row: Int, col: Int) = {
    state.synchronized {
      val added = state.reverse(row, col)
      if(added) activeZone.stretch(row, col)
      if(worldListener != null) worldListener.onChangeGeneration(state, activeZone)
    }
  }

  /** Генерирует на поле точки в случайных позициях
   */
  def randomiseState() = {
    state.synchronized {
      for( c <- (0 to worldSize * worldSize / 2).par ) {
        val row = Random.nextInt(worldSize)
        val col = Random.nextInt(worldSize)
        state.add(row, col)
        activeZone.stretch(row, col)
      }
      if(worldListener != null) worldListener.onChangeGeneration(state, activeZone)
    }
  }

  /** Производит вычисление состояния поля в следующем поколении.<br>
   */
  def makeDecisions() = {
    state.synchronized {

      val processors: Int = Runtime.getRuntime.availableProcessors()
      val futures = new Array[Future[Unit]](processors)

      for (p <- 0 until processors) {
        val future = Future[Unit] {
          processRows(p, processors)
        }
        futures(p) = future
      }

      try {
        for (f <- futures) Await.ready(f, Duration(worldSize, TimeUnit.MILLISECONDS))
      } catch {
        case e: TimeoutException =>
          println("To many time for make decision threads.")
          nextState.clear()
          nextActiveZone.collapse()
      }
    }
  }

  private def processRows(firsRow : Int, step: Int){

      for (row <- activeZone.left + firsRow to activeZone.right by step; col <- activeZone.top to activeZone.bottom) {
        val neighbours = sumNeighbours(row, col)
        val isAlive = state(row, col)

        if (isAlive) {nextState.add(row, col); nextActiveZone.stretch(row, col)}

        if (isAlive && (neighbours < 2 || 3 < neighbours)) nextState.remove(row, col)
        else if (!isAlive && neighbours == 3) {nextState.add(row, col); nextActiveZone.stretch(row, col)}

        //if(neighbours > 0) println("I am row:" + row + " col:" + col + " isAlive:" + isAlive + " neighbours:" + neighbours)
      }
      //println("state:" + state + " nextState:" + nextState)
  }

  private def sumNeighbours(row: Int, col: Int): Int = {
    var neighbours = 0
    if (row > 0             && col > 0             && state(row - 1, col - 1)) neighbours = neighbours + 1
    if (                       col > 0             && state(row,     col - 1)) neighbours = neighbours + 1
    if (row < worldSize - 1 && col > 0             && state(row + 1, col - 1)) neighbours = neighbours + 1
    if (row > 0                                    && state(row - 1, col    )) neighbours = neighbours + 1
    if (row < worldSize - 1                        && state(row + 1, col    )) neighbours = neighbours + 1
    if (row > 0             && col < worldSize - 1 && state(row - 1, col + 1)) neighbours = neighbours + 1
    if (col < worldSize - 1                        && state(row,     col + 1)) neighbours = neighbours + 1
    if (row < worldSize - 1 && col < worldSize - 1 && state(row + 1, col + 1)) neighbours = neighbours + 1
    neighbours
  }

  def releaseDecisions() {
    state.synchronized {
      state.cloneFrom(nextState)
      nextState.clear()
      activeZone.copyFrom(nextActiveZone)
      nextActiveZone.collapse()
      if(worldListener != null) worldListener.onChangeGeneration(state, activeZone)
    }
  }

}

object GameController {
  /**
   * Билдер, позволяющий задать исходное состояние поля
   * @param worldListener слушатель событий состояния поля
   * @param worldSize размерность игрового поля
   * @param spawns пары координат исходных позиций
   * @return созданный объект GameController
   */
  def apply(worldListener: WorldListener, worldSize: Int, spawns: Pair[Int, Int]*): GameController = {
    val gc = new GameController(worldListener, worldSize)
    for ((x, y) <- spawns.par) gc.reverseState(x, y)
    gc
  }
}

trait WorldListener {
  def onChangeGeneration(generation: BitSetWrapper, activeZone: ActiveZone): Unit
}