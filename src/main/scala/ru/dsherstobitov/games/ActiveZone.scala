package ru.dsherstobitov.games

/**
 * Created by dsherstobitov<br>
 * Date 03.11.14<br>
 * Активная зона игры
 * @param maxWidth максимальная ширина игрового поля
 * @param maxHeight максимальная высота игрового поля
 */
class ActiveZone(val maxWidth: Int, val maxHeight: Int) {
  private var minRow, maxRow, minCol, maxCol = -1

  def stretch(row: Int, col: Int):Unit = {
    if(minRow == -1) {minRow = row; maxRow = row; minCol = col; maxCol = col; return}

    minRow = minRow.min(row)
    maxRow = maxRow.max(row)
    minCol = minCol.min(col)
    maxCol = maxCol.max(col)

//    println("minRow:" + minRow + " maxRow:" + maxRow + " minCol:" + minCol + " maxCol:" + maxCol)
//    println("left:" + left + " right:" + right + " top:" + top + " bottom:" + bottom)
  }

  def collapse() = {minRow = -1; maxRow = -1; minCol = -1; maxCol = -1}

  def left: Int = 0.max(minRow - 1)
  def right: Int = maxWidth.min(maxRow + 1)
  def top: Int = 0.max(minCol - 1)
  def bottom: Int = maxHeight.min(maxCol + 1)

  def copyFrom(from : ActiveZone) = {
    minRow = from.minRow
    maxRow = from.maxRow
    minCol = from.minCol
    maxCol = from.maxCol
  }
}