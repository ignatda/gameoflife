package ru.dsherstobitov.games

import scala.collection.mutable

/**
 * Created by dsherstobitov<br>
 * Date 31.10.14<br>
 *
 * Обёртка, позволяющая работать с BitSet как с двумерным массивом
 */
class BitSetWrapper(protected var initSize: Int) {

  var bitSet = new mutable.BitSet(initSize * initSize)
  /** Добавить бит по указанным координатам
   * @param row индекс строки
   * @param col индекс столбца
   * @return true, если бит не был установлен до добавления
   */
  def add(row: Int, col: Int):Boolean = bitSet.add(row * initSize + col)

  /** Изменить состояние бита на противоположное
   * @param row индекс строки
   * @param col индекс столбца
   * @return новое состояние бита - true, если установлен
   */
  def reverse(row: Int, col: Int):Boolean = {
    if ( bitSet.apply(row * initSize + col) ) {
      remove(row, col); false
    } else {
      add(row, col); true
    }
  }

  /** Сбрасывает состояние бита по указанным координатам
    * @param row индекс строки
    * @param col индекс столбца
    * @return true, если бит был установлен до удаления
    */
  def remove(row: Int, col: Int):Boolean = bitSet.remove(row * initSize + col)

  /** Возвращает состояние бита по указанным координатам
   * @param row индекс строки
   * @param col индекс столбца
   * @return установлен ли бит
   */
  def apply(row: Int, col: Int) = bitSet.apply(row * initSize + col)

  def cloneFrom(from : BitSetWrapper) = {
    this.bitSet = from.bitSet.clone()
  }

  /** Сбрасывает все значения битов в ноль
   */
  def clear() = bitSet.clear()

  override def toString() = {
    var ret: String = "["
    for (row <- 0 until initSize; col <- 0 until initSize) if(apply(row, col)) ret = ret + "(" + row + ", " + col + ")"
    ret = ret + "]"
    ret
  }
}

