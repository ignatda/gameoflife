package ru.dsherstobitov.games

import java.awt.event._
import java.awt._
import javax.swing._
import javax.swing.event.{MouseInputAdapter, ChangeEvent, ChangeListener}

/**
 * Author: dsherstobitov<br>
 * Date:   31.10.2014.
 */
object MainFrame {
  val DRAG_CURSOR: Cursor = new Cursor(Cursor.MOVE_CURSOR)
  val DEFAULT_CURSOR: Cursor = new Cursor(Cursor.DEFAULT_CURSOR)
}

class MainFrame(val guiListener: GuiListener, val worldSize: Int) extends JFrame {

  private val source = new BitSetWrapper(worldSize)
  private val activeZone = new ActiveZone(worldSize, worldSize)

  private var cellSize = 8
  private var frameSize = worldSize * cellSize
  private var isPaused = true

  setLayout(new BorderLayout)

  val playButton: JButton = new JButton("Play")
  playButton.addActionListener(new ActionListener {
    def actionPerformed(e: ActionEvent) {
      isPaused = !isPaused
      e.getSource.asInstanceOf[JButton].setText(if (isPaused) "Play" else "Pause")
      guiListener.onPlayButton(isPaused)
    }
  })

  val randomButton: JButton = new JButton("Random Spawn")
  randomButton.addActionListener(new ActionListener {
    def actionPerformed(e: ActionEvent) {
      new SwingWorker[Void, Void] {
        protected def doInBackground(): Void = {
          guiListener.onRandomCall()
          null
        }
      }.execute()
    }
  })

  val refreshLabel: JLabel = new JLabel("Frequency: 50")
  val refreshSlider: JSlider = new JSlider(50, 1000, 50)
  refreshSlider.addChangeListener(new ChangeListener {
    def stateChanged(e: ChangeEvent) {
      refreshLabel.setText("Frequency: " + refreshSlider.getValue)
      if (!refreshSlider.getValueIsAdjusting) guiListener.onRefreshTimeChanged(refreshSlider.getValue, isPaused)
    }
  })

  val menuPanel = new JPanel
  menuPanel.add(playButton)
  menuPanel.add(randomButton)
  menuPanel.add(refreshLabel)
  menuPanel.add(refreshSlider)

  private val scrollableBoard = new ScrollableBoard
  private val viewPort = scrollableBoard.getViewport

  add(menuPanel, BorderLayout.NORTH)
  add(scrollableBoard, BorderLayout.CENTER)

  setPreferredSize(new Dimension(1024, 768))
  pack()

  def setDrawSource(source: BitSetWrapper, activeZone: ActiveZone) {
    this.source.cloneFrom(source)
    this.activeZone.copyFrom(activeZone)
    viewPort.repaint()
  }

  protected class ScrollableBoard extends JScrollPane {

    private val viewportView = new ScalableComponent
    viewportView.setPreferredSize(new Dimension(frameSize, frameSize))
    setViewportView(viewportView)

    viewportView.addMouseMotionListener(new MouseMotionListener {

      private var x = 0
      private var y = 0
      private var isDrag = false

      def mouseDragged(e: MouseEvent) {

        if (SwingUtilities.isLeftMouseButton(e)) return
        val viewportView: JComponent = getViewport.getView.asInstanceOf[JComponent]

        if (!isDrag) {
          isDrag = true
          x = e.getX
          y = e.getY
          viewportView.setCursor(MainFrame.DRAG_CURSOR)
          return
        }

        val bounds: Rectangle = getViewport.getViewRect
        val dx: Int = x - e.getX
        val dy: Int = y - e.getY
        bounds.translate(dx, dy)
        viewportView.scrollRectToVisible(bounds)
        x = e.getX + dx
        y = e.getY + dy
      }

      def mouseMoved(e: MouseEvent) {
        if (isDrag) {
          isDrag = false
          val viewportView: JComponent = getViewport.getView.asInstanceOf[JComponent]
          viewportView.setCursor(MainFrame.DEFAULT_CURSOR)
        }
      }
    })

    viewportView.addMouseListener(new MouseInputAdapter {
      override def mouseReleased(e: MouseEvent) {
        if (SwingUtilities.isRightMouseButton(e)) {
          if (e.getClickCount > 1) setExtendedState(getExtendedState | Frame.MAXIMIZED_BOTH)
        }
        else if (SwingUtilities.isLeftMouseButton(e)) {
          guiListener.onCellSwitch(e.getY / cellSize, e.getX / cellSize)
        }
      }
    })

    getViewport.addMouseWheelListener(new MouseWheelListener {
      def mouseWheelMoved(e: MouseWheelEvent) {
        if (e.getWheelRotation == -1) {
          if (cellSize < 32) rescale(2.0, e.getPoint)
        }
        else {
          if (cellSize > 1) rescale(0.5, e.getPoint)
        }
      }
    })

    private def rescale(scale: Double, mousePoint: Point) {
      cellSize = (cellSize * scale).toInt
      frameSize = worldSize * cellSize

      val bounds: Rectangle = getViewport.getViewRect
      bounds.translate(mousePoint.x, mousePoint.y)
      bounds.x = (bounds.x * scale).toInt
      bounds.y = (bounds.y * scale).toInt
      bounds.translate(-mousePoint.x, -mousePoint.y)

      val viewportView = getViewport.getView.asInstanceOf[JComponent]
      viewportView.setPreferredSize(new Dimension(frameSize, frameSize))
      viewportView.revalidate()
      viewportView.setLocation(-bounds.x, -bounds.y)
    }
  }

  protected class ScalableComponent extends JComponent {

    protected override def paintComponent(g: Graphics) {
      super.paintComponent(g)
      val viewRect: Rectangle = viewPort.getViewRect
      val g2: Graphics2D = g.asInstanceOf[Graphics2D]

      ////draw background
      g2.setColor(Color.DARK_GRAY.darker)
      g2.fillRect(viewRect.x, viewRect.y, viewRect.width, viewRect.height)

      //draw grid
      if (cellSize > 4) {
        g2.setColor(Color.DARK_GRAY)
        for (step <- cellSize until frameSize by cellSize) {
          g.drawLine(0, step, frameSize, step)
          g.drawLine(step, 0, step, frameSize)
        }
      }

      //draw cells
      g2.setColor(Color.YELLOW)
      for (row <- 0 until worldSize; col <- 0 until worldSize) {
        if (source.apply(row, col))
          if (viewRect.contains(col * cellSize + cellSize / 2, row * cellSize + cellSize / 2))
            g.fillRect(col * cellSize, row * cellSize, cellSize, cellSize)
      }

      //draw active zone
      g2.setColor(Color.GREEN.brighter)
      g2.drawRect(
        activeZone.top * cellSize,
        activeZone.left * cellSize,
        (activeZone.bottom + 1) * cellSize - activeZone.top * cellSize,
        (activeZone.right + 1) * cellSize - activeZone.left * cellSize
      )

      //draw board
      g2.setStroke(new BasicStroke(4))
      g2.setPaint(new GradientPaint(0, 0, Color.RED, 2, 2, Color.DARK_GRAY, true))
      g.drawRect(2, 2, frameSize - 4, frameSize - 4)
    }
  }

}

trait GuiListener {
  def onPlayButton(isPaused: Boolean)
  def onCellSwitch(row: Int, col: Int)
  def onRandomCall()
  def onRefreshTimeChanged(time: Int, isPaused: Boolean)
}
